<section class="main-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 p-0 bg-image">
                    <div class="d-flex justify-content-center align-items-center">
                        <h1 class="text-capitalize login-here fw-bold">login here</h1>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="custom-padding ps-5">
                        <h1 class="mt-3 sign-up mb-5">sign up</h1>
                        <div class="line "></div>
                        <div class="position-relative">
                            <div class="position-absolute circle-positiin">
                                <div class="circle">
                                    <div class="d-flex justify-content-center align-items-center h-100">
                                        <p class="text-white text-center mb-0">01</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="position-relative">
                            <div class="position-absolute circle-positiin-2">
                                <div class="circle-2">
                                    <div class="d-flex justify-content-center align-items-center h-100">
                                        <p class="text-white text-center mb-0">02</p>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="position-relative">
                            <div class="position-absolute circle-positiin-3">
                                <div class="circle-3">
                                    <div class="d-flex justify-content-center align-items-center h-100">
                                        <p class="text-white text-center mb-0">03</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="mt-5">
                            <div class="container-fluid">
                                <input type="email" class="form-control rounded-0 mb-3 text-uppercase ps-0"
                                    id="exampleFormControlInput1" placeholder="profile type">
                                <input type="email" class="form-control rounded-0 mb-3 text-uppercase ps-0"
                                    id="exampleFormControlInput1" placeholder="first name">
                                <input type="email" class="form-control rounded-0 mb-3 text-uppercase ps-0"
                                    id="exampleFormControlInput1" placeholder="last name">
                                <input type="email" class="form-control rounded-0 mb-3 text-uppercase ps-0"
                                    id="exampleFormControlInput1" placeholder="user name">
                                <input type="email" class="form-control rounded-0 mb-3 text-uppercase ps-0"
                                    id="exampleFormControlInput1" placeholder="email">
                                <input type="email" class="form-control rounded-0 mb-3 text-uppercase ps-0"
                                    id="exampleFormControlInput1" placeholder="confirm mail">
                                <input type="email" class="form-control rounded-0 mb-3 text-uppercase ps-0"
                                    id="exampleFormControlInput1" placeholder="password">
                                <input type="email" class="form-control rounded-0 mb-3 text-uppercase ps-0"
                                    id="exampleFormControlInput1" placeholder="cconfirm password *">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center align-items-center ">
                        <button type="button" class="fw-bold btn btn-color px-5 text-white mt-3 mb-3">sign up</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>